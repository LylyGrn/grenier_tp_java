//Exercice 2:

public class Variables {
	
	public static void main(String[] args){
		double a = 3.0; //Le type a �t� chang� de float � double car ces deux types ne sont pas correspondant et cr�ent une erreur (erreur de cast car deux types diff�rents)
		double b = 4;
		double c;
		
		c = Math.sqrt(a * a + b * b);
		System.out.println("c = " + c);
	}
}
// Ce programme permet de calculer la racine carr�e (Math.sqrt) du calcul contenu dans la variable c