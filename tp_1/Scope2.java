// Exercice Programme Scope

public class Scope2 {
	
	public static void main(String[] args){ 
		int nombre = 0; 
		for (int i = 0; i<4; i++){
			nombre++;
			System.out.print(nombre + ", ");
		}
		System.out.print(++nombre + ". ");
		System.out.print("\n");
	}
		
}
